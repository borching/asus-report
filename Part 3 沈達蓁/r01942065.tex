\documentclass[12pt,a4paper,onecolumn]{article}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb} %ams系列都是數學式子專用
\usepackage{makeidx} %用來生目錄(index)的
\usepackage{graphicx} %用來排版圖的

\begin{document}

\fontsize{12}{24pt}\selectfont
\title{Massive MIMO Downlink Transmission
Based on Random Beamforming CSI Estimation
} %寫在titlepage上的標題
\author{Student:Da-Chen Shen \\ Advisor: Hsuan-Jung Su}
\maketitle %用來生titlepage的

\pagenumbering{roman}

%\listoftables


\pagenumbering{arabic}

\section{Introduction} %標明章，會自動計算編號
\subsection{Background}
Multiple-input multiple-output (MIMO) technology is maturing and is
being incorporated into emerging wireless broadband standards like LTE
since spatial multiplexing can increase spectral efficiency. For example,
the LTE standard allows for up to 8 antenna ports at the base station.
Recently, Massive-MIMO has been put forward as a technology option for
realizing high-throughput green radios in beyond 4G wireless communication
systems. The concept of Massive-MIMO is a system where a base station
equipped with a large number of antennas, say 100 antennas or more,
simultaneously serves several users in the same frequency band \cite{concept}.
Theoretically, Massive-MIMO systems can almost perfectly alleviate the
inter-user interference that occurs in downlink multiuser MIMO (MU-MIMO)
systems with a simple linear precoder \cite{performance}, such as zero-forcing
or matched filter. The basic idea is that when the number of BS antennas
is large, the channel vector of any two users will be almost uncorrelated
and the instantaneous channel gain of each user can be well approximated
(with high probability) from its second order statistics.

To harvest the potential benefits of Massive-MIMO, the transmitter must
acquire the instantaneous channel state information (CSI) in order to
reduce the inter-user interference. In frequency division duplexing (FDD)
operation, each user estimates the channels based on the downlink training,
and then it feeds back its channel estimates to the BS through the reverse
link. However, in Massive-MIMO, the number of BS antennas is very large
and channel estimation becomes challenging in FDD since the number of
downlink resources needed for pilots will be proportional to the number
of BS antennas. Also, the required bandwidth for CSI feedback grows
linearly with the number of BS antennas and the number of users. By
contrast, in time division duplexing (TDD) systems, owing to the
channel reciprocity, the BS can obtain CSI from the uplink training.
The pilot transmission overhead is thus proportional to the number of
users which is typically much smaller than the number of BS antennas.
Therefore, CSI acquisition at the BS via open-loop training under TDD
operation is preferable in Massive-MIMO \cite{TDD1}-\cite{TDD5}.

Nonetheless, since the uplink and downlink channels between any two transceivers
is a product of ($i$) the frequency response of the transmit hardware, ($ii$)
the physical wireless channel, and ($iii$) the frequency response of the
receive hardware, the uplink and downlink channels are not reciprocal due to
the random phase and amplitude differences in the RF hardware. Therefore,
downlink CSI estimation based on the open-loop approach also requires pilots
to be sent between every base station antenna and terminal pair, as well
as feedback from each terminal \cite{reciprocity1}.  In practice, because of
calibration error in the downlink/uplink RF hardwares, the downlink channel
estimated by the uplink channel using channel reciprocity may not be accurate
\cite{reciprocity2}. In addition, FDD is generally
considered to be more effective for systems with symmetric traffic or
delay-sensitive applications and dominates current cellular systems. Thus,
we focus on a downlink training framework for FDD Massive-MIMO systems.

In order to make Massive-MIMO work for FDD operation, we apply a CSI
estimation method proposed in \cite{FDD1}\cite{FDD2} that uses CSI
estimation beam weights, the number of which is much less than the
number of transmit antennas, result in reducing the number of pilot
symbols and lightening the feedback overhead. We further propose a novel
scheme to reduce feedback load at the scheduling stage. The basic idea of
our proposed scheme come from \cite{scheduling}. Let users infer the scheduling
decision and guess its most possible rank among all the users, if its rank is high
enough to make its chance to be scheduled high, it feeds back its information.
Otherwise the user does not feedback in order to save the reverse link resource.
This way, the users opportunistically reduce the feedback load.
\\

\subsection{Previous Work}
Massive-MIMO is still in its infancy because there are a number of unanswered
technical questions. Massive-MIMO networks will be structured by the following
key elements: antennas, electronic components, network architectures, protocols
and signal processing \cite{concept}. The main focus of this article is signal
processing. The majority of the research on Massive-MIMO has focused on systems
that leverage time division duplexing (TDD) for two-way communication \cite{TDD1}-\cite{TDD5}.
There is also interest in employing massive MIMO with frequency division
duplexing (FDD) \cite{FDD1}-\cite{FDD3}, but there has very little academic work
in this area.

In \cite{concept}, it describes that the number of users can be simultaneously
served is limited, not by the number of antennas, but rather by our inability to
acquire CSI for an unlimited number of users. With channel
reciprocity, the receiving units send pilot symbols via uplink training. Since
the frequency response is assumed constant over $N_{Coh}$ subcarriers, $N_{Coh}$
users can transmit pilot symbols simultaneously during one OFDM symbol interval.
In total, this requires $K/N_{Coh}$ time slots ($K$ is the number of users served).
Thus, the channel coherent time fundamentally limits the number of users can
be simultaneously served. \cite{TDD5} also considers that when the number of users ($K$) is large, still
allocating $K$ symbols periods for pilot transmission may not be practically
feasible. Thus, \cite{TDD5} proposes a novel pilot optimization and channel
estimation algorithm in order to make it possible to reuse $N$ pilots among $K$ users ($N < K$).

In addition, \cite{TDD3} considers Massive-MIMO systems in cloud radio access networks (C-RAN).
In these networks, the digital unit (DU) processes the most physical layer/media
access control functions and generates in-phase and quadrature-phase data for
transmission at the radio unit (RU). The amount of IQ-data is proportional to the
number of antennas per RU, and the current DU-RU link rate is not sufficient to
support Massive-MIMO systems. Thus, the DU-RU wired link is a bottleneck due to
a large number of antennas or users served simultaneously. \cite{TDD3} also proposes
that users should be grouped according to their SNR value and weight vector
update interval. For example, \{cell-center, high speed\}, \{cell-center, low speed\},
\{cell-boundary, high speed\}, and \{cell-boundary, low speed\}. The number of
groups and criteria affect the sum-rate performance.

Without channel reciprocity, the conventional CSI feedback reduction methods,
such as vector quantization or codebook-based approaches, are known as a limited
feedback method for MIMO systems \cite{Feedback1}\cite{Feedback2}, may not be
suitable for Massive-MIMO as the codebook size has to be expanded extensively
(which leads to heavier feedback overhead) to capture fine-grain spatial channel structures.
Otherwise, the resultant quantization errors may not be acceptable for
schemes that are sensitive to inaccuracy in CSI estimates. This makes
codebook design for Massive-MIMO much more difficult.

A CSI feedback method based on compressive sensing for the Massive-MIMO
is proposed in \cite{FDD3}. Although method in \cite{FDD3}
compress the CSI feedback by utilizing the spatial correlation, the CSI
corresponding to all transmit antennas must be estimated at the receiver.
Thus, the base station transmits as many pilot symbols as there are transmit
antennas to accurately estimate the CSI in the downlink.

Channel estimation with training sequences of length less than the number of
transmit antennas is considered in \cite{FDD1}\cite{FDD2}. The proposed scheme
in \cite{FDD1}\cite{FDD2} first generates the CSI estimation beam weights
randomly and revises them using the obtained CSI that corresponds to the
limited number of CSI estimation beam weights. Finally, the performance of
the scheme approaches that of conventional full antenna search CSI estimation.

However, the feedback resource exploited to provide CSIT was used at once for
precoding matrix design to serve the selected users in \cite{FDD1}\cite{FDD2}.
In our work, we propose that the feedback process comprises
two stages: A first stage devoted to scheduling followed by a second stage
for precoder design for the selected users. In the first stage, the scheduling
algorithm might get away with a poorer representation of the channel, so we propose
a novel scheme to reduce feedback load. In the second stage, we apply the approach
proposed in \cite{FDD1}\cite{FDD2} to design the precoding matrix for the selected users.
\\

\subsection{Notations}
Throughout the paper, vectors and matrices are denoted in bold-face lower
and upper cased, respectively. For matrix $\mathbf{A}$, $\mathbf{A}^H$ denote
the Hermitian operations. $\mathbf{A}^{-1}$ and $\| \mathbf{A}\|$ denotes the
pseudo inverse of $\mathbf{A}$ and Frobenius norm of $\mathbf{A}$, respectively.
$\mathbf{I}_N$ is the identity matrix of dimension $N$. $E[\cdot]$ denotes the
expectation operator.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{System Model and Problem Formulation}

\subsection{System Model}
The MU-MIMO downlink system model is shown in Figure \ref{DL}.

%
\begin{figure}[htbp] %圖形環境。[htbp]是重要的簡便參數，必加!!
\centering
\includegraphics[width=\textwidth]{DL}\\
\caption{Downlink multiuser MIMO system model}\label{DL}
\end{figure}
%

The BS is equipped with $M_{t}$ antennas. There are $U$ users in the system and each user has $M_{r}$
antennas. We consider a full buffer traffic model, which means each user always has
data in the buffer to transmit. The two steps of scheduling (finding the optimal $K$ users)
and precoding matrix design are mapped into two feedback stages.
Thanks to the Massive-MIMO systems have ability to separate any two users,
the scheduler don't need the channel direction information (CDI) to select users that
satisfy the semi-orthogonal constraint to each other. Instead, the scheduling algorithm
only needs the channel quality indicator (CQI) to get away with a poorer representation of the channel.
Therefore, each of the $U$ users feeds back the  CQI to the BS in the first stage.
Then, the scheduled $K$ users feed back the CDI which is used to design the precoding matrix to the BS in the second stage.

The CSI estimation method proposed in \cite{FDD1}\cite{FDD2} is shown in Figure \ref{RB_CSI}, where
only concerned with $K$ selected users that the BS simultaneously communicated with.
The downlink CSI at the $k$-th user is represented by channel matrix $\mathbf{H}_{k}\in\mathcal{C}^{M_r \times M_{t}}$.
To reduce the number of the pilot symbols for CSI estimation in the downlink, the BS transmits
$N_b$ beams to the users for CSI estimation by using CSI estimation weights $\mathbf{T}_{b}^{(n)}\in\mathcal{C}^{M_t \times N_{b}}$,
where $(n)$ denotes how many CSI estimations have been conducted.

The number of beams for CSI estimation $N_b$ is defined as
%
\begin{equation}\label{beams number}
N_b = N_{add} + KM_{r}
\end{equation}
%
where $N_{add}$ is the number of redundant beams. By using redundant beams, this
scheme gradually discovers the CSI for the entire transmission space.

The received signal matrix at $k$-th user for the $n$-th CSI estimation, $\mathbf{Y}_{0,k}^{(n)}\in\mathcal{C}^{M_r \times N_{b}}$,
is given by
%
\begin{equation}\label{received_matrix}
\mathbf{Y}_{0,k}^{(n)}= \mathbf{H}_{k}\mathbf{T}_{b}^{(n)}\mathbf{X}_{0}+\mathbf{n}_{0,k}
\end{equation}
%
where $\mathbf{X}_{0}$ is an $N_b \times N_b$ diagonal matrix. The norm of each column vector of $\mathbf{T}_{b}^{(n)}$
is set to satisfy $\| \mathbf{t}_{b,i}^{(n)}\|=P$. The noise variance of $\mathbf{n}_{0,k}\in\mathcal{C}^{M_r \times N_{b}}$ is $\sigma^{2}$.
Since the user knows $\mathbf{X}_{0}$, the subspace channel matrix corresponding to CSI
estimation beams for $k$-th user, $\mathbf{G}_{k}^{(n)}\in\mathcal{C}^{M_r \times N_{b}}$,
is estimated as
%
\begin{equation}\label{estimated_beam_channel_matrix}
\mathbf{G}_{k}^{(n)}=\mathbf{Y}_{0,k}^{(n)}\mathbf{X}_{0}^{-1}=\mathbf{H}_{k}\mathbf{T}_{b}^{(n)}+\mathbf{n}_{0,k}^{'}
\end{equation}
%
where the noise variance of the elements in the noise matrix, $\mathbf{n}_{0,k}^{'}$ is also expressed as $\sigma^{2}$,
since $\mathbf{X}_{0}$ is unitary matrix.

The $k$-th user calculates the singular value decomposition (SVD) of $\mathbf{G}_{k}^{(n)}$ as in
%
\begin{equation}\label{SVD}
\mathbf{G}_{k}^{(n)} =  \mathbf{U}_{k}^{(n)} \, (\mathbf{\Sigma}_{k}^{(n)} \,\,\, 0) \, (\mathbf{V}_{S,k}^{(n)} \,\,\, \mathbf{V}_{N,k}^{(n)})^{H}
\end{equation}
%
where $\mathbf{U}_{k}^{(n)}\in\mathcal{C}^{M_r \times M_{r}}$ and $(\mathbf{V}_{S,k}^{(n)} \,\,\, \mathbf{V}_{N,k}^{(n)})\in\mathcal{C}^{N_b \times N_{b}}$
are the left and right singular vectors, respectively, $\mathbf{\Sigma}_{k}^{(n)}$ is the $M_r \times M_r$ diagonal matrix whose diagonal elements
are square roots of the eigenvalues: $\sqrt{\lambda_{k,1}^{(n)}}$ , ... , $\sqrt{\lambda_{k,M_r}^{(n)}}$.

The subspace unitary matrix for feedback is obtained by multiplication of the right and left singular vectors
of the subspace channel matrix. The subspace unitary matrix, $\mathbf{V}_{P,k}^{(n)}\in\mathcal{C}^{N_b \times M_{r}}$,
is expressed as
%
\begin{equation}\label{feedback_unitary_matrix}
\mathbf{V}_{P,k}^{(n)} =  \mathbf{V}_{S,k}^{(n)} \, {\mathbf{U}_{k}^{(n)}}^{H}
\end{equation}
%
We can see the Hermitian transposition of $\mathbf{V}_{P,k}^{(n)}$, $\mathbf{U}_{k}^{(n)}{\mathbf{V}_{S,k}^{(n)}}^{H}$,
is identical with $\mathbf{G}_{k}^{(n)}$ when $\mathbf{\Sigma}_{k}^{(n)}$ is an identity matrix. The differences
between $\mathbf{G}_{k}^{(n)}$ and $\mathbf{V}_{P,k}^{(n)}$ are caused by the distribution of eigenvalues.
%
\begin{figure}[htbp] %圖形環境。[htbp]是重要的簡便參數，必加!!
\centering
\includegraphics[width=\textwidth]{RB_CSI}\\
\caption{CSI estimation method based on random beamforming for Massive-MIMO}\label{RB_CSI} 
\end{figure}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{CSI Estimation Weight Design}
The random beamforming matrix $\mathbf{V}_{b}\in\mathcal{C}^{M_{t} \times M_{t}}$ is first calculated as
the right singular vectors of a random Gaussian channel matrix $\mathbf{R}_{G}\in\mathcal{C}^{M_{t} \times M_{t}}$.
The transmission weights for CSI estimation beams at the first step, $\mathbf{T}_{b}^{(1)}\in\mathcal{C}^{M_t \times N_{b}}$,
are given by
%
\begin{equation}\label{estimation_beam_design}
\mathbf{T}_{b}^{(1)}=( \mathbf{v}_{b,1} \mathbf{ \,\, ,\cdots, \,\, } \mathbf{v}_{b,N_b} )
\end{equation}
%
where $\mathbf{v}_{b,i}$ is the $i$-th column vector of $\mathbf{V}_{b}$.

The CSI estimation weight, $\mathbf{T}_{b}^{(n+1)}$, is calculated by the subspace unitary matrix, $\mathbf{V}_{P,k}^{(n)}$,
which is fed back from the users in the $n$-th CSI estimation and the random beam matrix $\mathbf{V}_{b}$. The CSI estimation
weight is calculated to maximize $\sum_{k=1}^K \| \mathbf{H}_{k}\mathbf{T}_{b}^{(n+1)}\|^{2}$.
Thus, the right singular vectors of the aggregate channel matrix $(\mathbf{H}_{1}^{T} \cdots \mathbf{H}_{K}^{T})^T$ are
optimal as the CSI estimation weight. The right singular vectors are given by the SVD of the aggregate channel matrix as
%
\begin{equation}\label{SVD_aggregate}
\left[
\begin{array}{cccc}
\mathbf{H}_{1}\\
\vdots \\
\mathbf{H}_{K}   \\
\end{array}
\right] = \mathbf{U}_{A} \, (\mathbf{\Sigma}_{A} \,\,\, \mathbf{0}) \, (\mathbf{V}_{SA} \,\,\, \mathbf{V}_{NA})^{H}
\end{equation}
%
When $\mathbf{V}_{SA}$ is identical to $\mathbf{T}_{b}^{(n+1)}$ and $det (\mathbf{V}_{SA}^{H}\mathbf{T}_{b}^{(n+1)})=1$,
$\sum_{k=1}^K \| \mathbf{H}_{k}\mathbf{T}_{b}^{(n+1)}\|^{2}$ is maximized and becomes $P\sum_{k=1}^K \| \mathbf{H}_{k}\|^{2}$.

To estimate $\mathbf{V}_{SA}$ of the aggregate channel matrix as the CSI estimation weight for the $(n+1)$-th CSI
estimation by using $\mathbf{V}_{P,k}^{(n)}$. The aggregate matrix $\mathbf{V}_{A}\in\mathcal{C}^{M_t \times KM_{r}}$
is defined as
%
\begin{equation}\label{aggregate_matrix}
\mathbf{V}_{A}^{(n)}=\mathbf{T}_{b}^{(n)}( \mathbf{V}_{P,1}^{(n)} \mathbf{ \,\, ,\cdots, \,\, } \mathbf{V}_{P,K}^{(n)} )
\end{equation}
%
And $N_{add}$ beams of the random beam matrix $\mathbf{V}_{b}$ are added to the CSI estimation beams to extend
the signal space. The additional $N_{add}$ beams weight matrix $\mathbf{E}^{(n)}\in\mathcal{C}^{M_t \times N_{add}}$
is defined as
%
\begin{equation}\label{additional_beams}
\mathbf{E}^{(n)}=( \mathbf{v}_{b,1+(N_b+(n-1)N_{add} \,\,\, mod \,\,\, M_t)} \mathbf{ \,\, ,\cdots, \,\, } \mathbf{v}_{b,1+(N_b+nN_{add}-1 \,\,\, mod \,\,\, M_t)} )
\end{equation}
%
To obtain the basis vectors, QR decomposition is conducted for $(\mathbf{V}_{A}^{(n)} \,\,\, \mathbf{E}^{(n)})$ as
%
\begin{equation}\label{QR}
(\mathbf{V}_{A}^{(n)} \,\,\, \mathbf{E}^{(n)})=
(\mathbf{Q}_{SA}^{(n)} \,\,\, \mathbf{Q}_{NA}^{(n)})
\left(
\begin{array}{cccc}
\mathbf{R}_{A}^{(n)}\\
\mathbf{0}   \\
\end{array}
\right)
\end{equation}
%
where $\mathbf{Q}_{SA}^{(n)}$ is the $M_t \times (KM_r+N_{add})$ matrix.
The CSI estimation weight is given by
%
\begin{equation}\label{CSI_estimation_weight}
\mathbf{T}_{b}^{(n+1)}= \sqrt{P} \mathbf{Q}_{SA}^{(n)}
\end{equation}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Data Transmission Weight Design}
$\mathbf{V}_{P,k}^{(n)}$ is used to calculate the transmission weight $\mathbf{W}_{k}^{(n)}\in\mathcal{C}^{M_t \times M_r}$.
Since the estimated CSI contains error, $\mathbf{V}_{A}^{(n)}$ cannot be used for the MU-MIMO
transmission weight but the single-user MIMO transmission or the CSI estimation weight.
In the BD algorithm, the null space vectors for the $k$-th user, are defined by QR decomposition as
%
\begin{equation}\label{QR_null}
(\mathbf{V}_{P,1}^{(n)} \,\cdots\, \mathbf{V}_{P,k-1}^{(n)} \,\, \mathbf{V}_{P,k+1}^{(n)} \,\cdots\, \mathbf{V}_{P,K}^{(n)})=
(\mathbf{Q}_{SW,k}^{(n)} \,\,\, \mathbf{Q}_{NW,k}^{(n)})
\left(
\begin{array}{cccc}
\mathbf{R}_{W,k}^{(n)}\\
\mathbf{0}   \\
\end{array}
\right)
\end{equation}
%
where the null space vectors, $\mathbf{Q}_{NW,k}^{(n)}\in\mathcal{C}^{N_b \times (N_b-(K-1)M_r)}$,
correspond to the zero matrix. The SVD is conducted for the channel matrix in the null space as
%
\begin{equation}\label{SVD_null}
\mathbf{V}_{P,k}^{(n)H} \mathbf{Q}_{NW,k}^{(n)}
= \mathbf{\widetilde{U}}_{P,k}^{(n)} \, (\mathbf{\widetilde\Sigma}_{P,k}^{(n)} \,\,\, \mathbf{0}) \, (\mathbf{\widetilde{V}}_{SP,k}^{(n)} \,\,\, \mathbf{\widetilde{V}}_{NP,k}^{(n)})^{H}
\end{equation}
%
where $\mathbf{\widetilde{V}}_{SP,k}^{(n)}\in\mathcal{C}^{(N_b-(K-1)M_r) \times M_r}$ denotes
the signal space of the $k$-th user channel matrix in the null space for the other users.
The transmission weight for $k$-th user $\mathbf{W}_{k}^{(n)}\in\mathcal{C}^{M_t \times M_r}$ is
determined as $\mathbf{T}_{b}^{(n)} \mathbf{Q}_{NW,k}^{(n)} \mathbf{\widetilde{V}}_{SP,k}^{(n)}$.
The norms of its column vectors are set to $\sqrt{P/KM_r}$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Opportunistic Feedback Reduction at Scheduling Stage}
We take the scheduling mechanism in the first stage into consideration, as shown in Figure \ref{DL2}.
In the previous statement, we can describe the subspace unitary matrix for feedback $\mathbf{V}_{P,k}^{(n)}\in\mathcal{C}^{N_b \times M_{r}}$
as the channel direction information (CDI).

%
\begin{figure}[htbp] %圖形環境。[htbp]是重要的簡便參數，必加!!
\centering
\includegraphics[width=\textwidth]{DL2}
\caption{Downlink multiuser MIMO system model}\label{DL2} 
\end{figure}
%

In the first stage, there are $U$ users in the system instead of $K$ users, as shown in Figure \ref{RB_CSI2}.
The subspace channel matrix corresponding to CSI estimation beams for $u$-th user,
$\mathbf{G}_{u}^{(n)}\in\mathcal{C}^{M_r \times N_{b}}$,
is estimated as
%
\begin{equation}\label{estimated_beam_channel_matrix}
\mathbf{G}_{u}^{(n)}=\mathbf{Y}_{0,u}^{(n)}\mathbf{X}_{0}^{-1}=\mathbf{H}_{u}\mathbf{T}_{b}^{(n)}+\mathbf{n}_{0,u}^{'}
\end{equation}
%
where the noise variance of the elements in the noise matrix, $\mathbf{n}_{0,u}^{'}$ is also expressed as $\sigma^{2}$,
since $\mathbf{X}_{0}$ is unitary matrix.
The $u$-th user calculates the singular value decomposition (SVD) of $\mathbf{G}_{u}^{(n)}$ as in
%
\begin{equation}\label{SVD}
\mathbf{G}_{u}^{(n)} =  \mathbf{U}_{u}^{(n)} \, (\mathbf{\Sigma}_{u}^{(n)} \,\,\, 0) \, (\mathbf{V}_{S,u}^{(n)} \,\,\, \mathbf{V}_{N,u}^{(n)})^{H}
\end{equation}
%
%
\begin{figure}[htbp] %圖形環境。[htbp]是重要的簡便參數，必加!!
\centering
\includegraphics[width=\textwidth]{RB_CSI2}
\caption{CSI estimation method based on random beamforming for Massive-MIMO}\label{RB_CSI2} 
\end{figure}
%

The channel quality indicator (CQI) can be described as $\| \mathbf{G}_{u}^{(n)}\|$, which is
equal to $\| \mathbf{\Sigma}_{u}^{(n)}\|$. However, the aggregated feedback load increased linearly
with respect $U$. In order to efficiently utilize the feedback resources at scheduling stage,
the proposed scheme divides the range of CQI into multiple regions according to the order statistics
of $\| \mathbf{G}_{u}^{(n)}\|$ which reflect the properties of scheduling. Each region corresponds
to a range $\| \mathbf{G}_{u}^{(n)}\|$ and is quantized with a specific number of bits (usually,
more quantization bits for regions with higher $\| \mathbf{G}_{u}^{(n)}\|$) to further assist
scheduling. The CQI feeback for scheduling thus consists of two parts: one indicating the region
that $\| \mathbf{G}_{u}^{(n)}\|$ falls in, and the other being the quantized result of that region.
When $\| \mathbf{G}_{u}^{(n)}\|$ is in the lowest region (that is, below the lowest threshold),
$u$-th user does not feedback at all. This way, the users opportunistically reduce the feedback load
at the scheduling stage, as shown in Figure \ref{CQI_FB}. A set of $N$ thresholds $R_{th}  =  \{r_{th,1}, r_{th,2}, \ldots , r_{th,N}\}$
is set according to the order statistics of $\| \mathbf{G}_{u}^{(n)}\|$.
%
\begin{figure}[htbp] %圖形環境。[htbp]是重要的簡便參數，必加!!
\centering
\includegraphics[width=\textwidth]{CQI_FB}
\caption{Multi-threshold feedback model}\label{CQI_FB} 
\end{figure}
%
\\
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion and Future Work}
\subsection{Conclusion}
We apply the approach proposed in \cite{FDD1}\cite{FDD2}, which is based on the iterative
CSI estimation scheme and calculated the CSI estimation weights using the unitary matrices
for the selected users, in order to make Massive-MIMO work for FDD systems.
In addition, we take the scheduling mechanism into consideration and propose a novel scheme
of opportunistic feedback reduction at scheduling stage.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Future Work}
We will derive the order statistics of $\| \mathbf{G}_{u}^{(n)}\|$ and find the set
of $N$ thresholds $R_{th}  =  \{r_{th,1}, r_{th,2}, \ldots , r_{th,N}\}$ in my future work.
\\
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\addcontentsline{toc}{chapter}{Bibliography}
\begin{thebibliography}{20}

\bibitem{concept} F. Rusek, D. Persson, B. K. Lau, E. G. Larsson, T. L. Marzetta,
 O. Edfors, and F. Tufvesson, ``Scaling up MIMO: opportunities and challenges with
 very large arrays," \emph{IEEE Signal Process. Mag.}, vol. 30, pp. 40-60, Jan. 2013.

\bibitem{performance} H. Yang and T. L. Marzetta, ``Performance of conjugate and
 zero-forcing beamforming in large-scale antenna systems," \emph{IEEE J. Sel. Areas
 Commun.},vol. 31, no. 2, pp. 172–179, Feb. 2013.

\bibitem{TDD1} H. Q. Ngo, E. G. Larsson, and T. L. Marzetta, ''Massive MU-MIMO
Downlink TDD Systems with Linear Precoding and Downlink Pilots", in Proc. Allerton
Conference on Communication, Control, and Computing, Urbana-Champaign, Illinois, Oct. 2013.

\bibitem{TDD2}J. Jose, A. Ashikhmin, P. Whiting, and S. Vishwanath, “Channel estimation
and linear precoding in multiuser multiple-antenna TDD systems,”
IEEE Trans. Veh. Technol., vol. 60, no. 5, pp. 2102–2116, Jun. 2011.

\bibitem{TDD3}Sangkyu Park, Chan-Byoung Chae, and Saewoong Bahk,''Before/After
Precoding Massive MIMO Systems for Cloud Radio Access Networks", published in Journal
of Communication and Networks, vol. 15, no. 4, AUG. 2013

\bibitem{TDD4}C. Shepard, H. Yu, N. Anand, E. Li, T. Marzetta, R. Yang, and L. Zhong,
''Argos: Practical many-antenna base stations," in Proc. ACM MobiCom, 2012.

\bibitem{TDD5}Tadilo Endeshaw Bogale and Long Bao Le, ''Pilot Optimization and Channel
Estimation for Multiuser Massive MIMO Systems",  accepted in CISS 2014 Conference.

\bibitem{reciprocity1} M. Guillaud, D. Slock, and R. Knopp, ''A Practical Method for Wireless
Channel Reciprocity Exploitation Through Relative Calibration," in Proc., ISSPA ’05, Sydney, Australia, 2005.

\bibitem{reciprocity2}J. Guey and L. D. Larsson, “Modeling and evaluation of MIMO systems
exploiting channel reciprocity in TDD mode,” Proceedings of IEEE Vehicular Technology Conference, Sep. 2004.

\bibitem{FDD1}R. Kudo, J. McGeehan, S. M. D. Armour, and M. Mizoguchi, ''CSI estimation
method based on random beamforming for massive number of transmit antenna systems,"
in Proc., ISWCS, Aug. 2012.

\bibitem{FDD2}R. Kudo , S. M. D. Armour , J. McGeehan and M. Mizoguchi, ''A Channel
State Information Feedback Method for Massive MIMO-OFDM," Journal of Communications
and Networks.2013.

\bibitem{FDD3}Ping-Heng Kuo, H. T. Kung, and Pang-AN Ting, ''Compressive sensing
based channel feedback protocols for spatially-correlated massive antenna arrays," in Proc.,
IEEE WCNC, Apr. 2012.

\bibitem{scheduling}Jin-Hao Li and Hsuan-Jung Su ,''Opportunistic feedback reduction for multiuser
 MIMO broadcast channel with orthogonal beamforming", IEEE TRANSACTIONS on WIRELESS COMMUNICATIONS,
 accepted for publication.

\bibitem{Feedback1}D. J. Love, R. W. Heath, V. K. N. Lau, D. Gesbert, B. D. Rao, and
M. Andrews,''An overview of limited feedback in wireless communication systems,"
IEEE J. Select. Areas Commun., vol. 26, pp. 1341–1365, Oct. 2008.

\bibitem{Feedback2} K. Schober, R. Wichman and T. Koivisto, ''MIMO adaptive codebook
for closely spaced antenna arrays," in Proc. European Signal Processing
Conference (EUSIPCO),2011, pp. 1-5.

\end{thebibliography}


\end{document}
