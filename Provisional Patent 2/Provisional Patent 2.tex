\documentclass[10pt,a4paper]{article}
\usepackage[margin=1.5cm]{geometry}
\usepackage{graphicx}
\usepackage{amsmath}

\begin{document}
\title{\Large\bf \begin{center}Method of Resource Allocation for Device-to-Device (D2D)\\
                 Discovery/Communication Signal Transmission in Cellular Networks \end{center} }
\author{Yen-Ming Huang and Borching Su}
\date{\today}
\maketitle

\section{Background}
\hspace*{1em}In \cite{patent}, we have proposed a method of resource allocation for device-to-device (D2D) discovery/communication signal transmission to deal with three known issues that affect the performance of D2D operation: half duplex constraint, near-far problem causing the in-band emission, and deep fade in D2D channel links. In this provisional patent, we further consider the issue of co-channel interference of D2D signal transmission stemmed from reusing the radio resources by the UEs in other macro sites. Refer to \cite{36843}, a macro site (MS) consists of three sectors, UEs therein are assumed to follow the same strategy of resource allocation for their D2D signals transmission.

\section{The Description of Issues}
\hspace*{1em}Consider the case that the UEs transmit D2D discovery/communication signals in different macro sites but use the same radio resource, there exists the issue of co-channel interference of D2D signal transmission. As shown in Figure \ref{2sites_3UEs}, UE C receives the D2D signal from UE A and meanwhile suffers the co-channel interference from UE B. Suppose that the received power of this interference is comparable with the received power of desired signal for UE C, it leads to very poor detection performance on UE C. Since there are multiple opportunities of D2D signal transmission for each UE, one design guideline of resource allocation method is to ensure that the interference does not come solely from one UE as shown in Figure \ref{goodalloc}. UE C may never successfully receive the signal from UE A if adopting the resource allocation as shown in Figure \ref{badalloc}.
\begin{figure}[htp]
\centering \centerline{
\includegraphics[width=0.4\textwidth,clip]{./2sites_3UEs.eps}}
\caption {An illustration of co-channel interference of D2D signal transmission}
\label{2sites_3UEs}
\end{figure}
\begin{figure}[htp]
\centering \centerline{
\includegraphics[width=0.5\textwidth,clip]{./goodalloc.eps}}
\caption {The interference not solely from one UE}
\label{goodalloc}
\end{figure}
\begin{figure}[htp]
\centering \centerline{
\includegraphics[width=0.5\textwidth,clip]{./badalloc.eps}}
\caption {The interference solely from one UE}
\label{badalloc}
\end{figure}

\section{The Description of Invention}
\hspace*{1em}Refer to \cite{patent}, we use two terms of D2D resource block (DRB) and D2D resource pattern (DRP) in this document. A DRB is the basic unit of resource for one UE (device) signal transmission in D2D discovery and D2D communication, whose size can be defined differently according to different requirements. A DRP consists of several DRBs. The proposed hopping mechanism in \cite{patent} for D2D discovery/communication signal transmission is recaptured as follows:
\[
    g_{l}(k)=[g_{l}(0)+p\cdot k\mod M]\mod M \\
\]
\[
    i_{l}(k)=[i_{l}(0)+(q\cdot k\cdot (g_{l}(0)+r)\mod N)]\mod N.
\]
Equivalently, we have
\begin{equation}
\label{eq1}
m_{l}(k)=[m_{l}(0)+p\cdot k]\mod M
\end{equation}
\begin{equation}
\label{eq2}
n_{l}(k)=[n_{l}(0)+q\cdot k\cdot (m_{l}(0)+r)]\mod N
\end{equation}
where\\
\hspace*{4em}$M$ is the number of frequency indices for DRBs to reside in a DRP,\\
\hspace*{4em}$N$ is the number of time indices for DRBs to reside in a DRP,\\
\hspace*{4em}$K$ is the number of DRPs in a complete D2D discovery/communication session,\\
\hspace*{4em}$L$ is the number of UEs transmitting signals in a DRP ($L\leq M\times N$),\\
\hspace*{4em}$m_{l}(k)$ is the frequency index of the DRB for the $l$-th UE  to transmit signal in the $k$-th DRP,\\
\hspace*{4em}$n_{l}(k)$ is the time index of the DRB for the $l$-th UE to transmit signal in the $k$-th DRP,\\
\hspace*{4em}$k$ is the index of DRP in the range $\{0,1,\cdots,K-1\},$\\
\hspace*{4em}$p$ is a positive integer which is relatively prime with $M$,\\
\hspace*{4em}$q$ is a positive integer which is relatively prime with $N$,\\
\hspace*{4em}$r$ is an integer which can be selected from the set $\{0,1,\cdots,N-1\}$.\\
Figure \ref{TFRS} gives an illustration of aforementioned items.\\
\begin{figure}[htp]
\centering \centerline{
\includegraphics[width=0.6\textwidth,clip]{./TFRS.eps}}
\caption {Time-frequency resource structure}
\label{TFRS}
\end{figure}
\hspace*{1em}As displayed in \cite{patent}, by rotating usage of DRBs in both time and frequency indices so that every UE naturally gets opportunities to be exposed to other UEs, the proposed hopping mechanism for the resource allocation can deal with the following three issues:
\begin{itemize}
\item
Half duplex constraint;
\item
Near-far problem causing the in-band emission;
\item
Deep fade in D2D channel links.
\end{itemize}
\hspace*{1em}We further consider the issue of co-channel interference of D2D signal transmission in cellular networks. To cope with this impediment in D2D discovery and D2D communication, the values of $r$ in the Equation \ref{eq2} are chosen to be different for the resource allocation of the UEs transmitting signals in the macro sites using the same resource pool. That is, a resource pool can be reused by the UEs in the other macro sites. Take a simple scenario as shown in Figure \ref{7sites_3reuse} for example, UEs transmitting signals in MS1, MS2, and MS3 use the same resource pool. The numbers in the hexagons in MS1, MS2, and MS3 denote the corresponding values of $r$ for their resource allocation. With the parameters $M=N=K=3, L=9$, and $p=q=1$, Figure \ref{M3N3K3L9DRP_3reuse} depicts the corresponding distribution of DRBs utilization based on the proposed hopping mechanism.
\begin{figure}[htp]
\centering \centerline{
\includegraphics[width=0.3\textwidth,clip]{./7sites_3reuse.eps}}
\caption {The corresponding values of $r$ for the resource allocation in the three macro sites}
\label{7sites_3reuse}
\end{figure}
\begin{figure}[htp]
\centering \centerline{
\includegraphics[width=0.5\textwidth,clip]{./M3N3K3L9DRP_3reuse.eps}}
\caption {The distribution of DRBs utilization for the three macro sites using the same resource pool}
\label{M3N3K3L9DRP_3reuse}
\end{figure}




\begin{thebibliography}{99}
\bibitem{patent}
61/883, 176, Method of resource allocation for device-to-device (D2D) discovery/communication signal transmission.
\bibitem{36843}
3GPP TR 36.843, ``Study on LTE device to device proximity services; radio aspects,'' V1.2.0, February, 2014.




\end{thebibliography}


\end{document} 